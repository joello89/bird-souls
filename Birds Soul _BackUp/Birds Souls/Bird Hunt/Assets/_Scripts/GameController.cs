﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int score;
    public Text Score_Text;
    public Text final_Text;
    public Text 
        finalScore_Text, 
        lvlText,
        lvl2Text,
        lvl3Text;

    [SerializeField] Timer timer;
    [SerializeField] SpawnManager1 spawnManager1;

    public GameObject PauseButton;
    public GameObject[] Levels = new GameObject[3];

    public AudioSource src;
    public AudioClip gunShot;
    public AudioClip bird;

    public int 
        Lvl1,
        Lvl2,
        Lvl3,
        FinalScore;

    public bool 
        complete1,
        complete2,
        complete3;

    private void Start(){
        score = 0;
        PauseButton.SetActive(false);
        Score_Text.text = score.ToString();
        final_Text.text = score.ToString();
    }

    public void PlayGunShot(){
        src.PlayOneShot(gunShot, 0.6f);
        Invoke("PlayBirdSound", 0.1f);
    }

    public void PlayBirdSound() {
        src.PlayOneShot(bird, 0.9f);
    }

    public void AddPoint(int amount) {
        score += amount;
        Score_Text.text = score.ToString();
        final_Text.text = score.ToString();
    }

    public void StartGame() {
        Time.timeScale = 1;
        timer.GameOver = false;
        PauseButton.SetActive(true);
        spawnManager1.StartSpawn();
     
    }
              
    public void Show(GameObject obj){
        obj.SetActive(true);
    }

    public void Hide(GameObject obj){
        obj.SetActive(false);
    }


    #region Game Functions

    public void PauseGame() {
        Time.timeScale = 0;
        timer.GameOver = true;
        PauseButton.SetActive(false);
    }

    public void RestartGame() {
        SceneManager.LoadScene("Level1");
    }

    public void ExitGame(){
        Application.Quit();
    }

    public void ActivateLevel(GameObject lvl) {

        lvl.SetActive(true);
    }

    public void NextLevel(string scene) {
        SceneManager.LoadScene(scene);
    }

    public void ShowFinalScore() {
        FinalScore += Lvl1 + Lvl2 + Lvl3;
        finalScore_Text.text = FinalScore.ToString();
    }

    public void DeactivateLevel(){

    }

    #endregion
}
