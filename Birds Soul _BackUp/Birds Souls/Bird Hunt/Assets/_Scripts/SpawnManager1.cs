﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager1 : MonoBehaviour
{

    // ARRAYS
    public GameObject[] Birds = new GameObject[8];

    public Sprite[] BirdSprite = new Sprite[8];

    [SerializeField] Timer timer;

    public int rand;
    public int index;

    public bool 
        seq1, 
        seq2,
        seq3,
        seq4,
        seq5 = false;

    void Start(){
        HideAllBirds();
    }

    public void StartSpawn() {
        if (timer.GameOver == false){
            //InvokeRepeating("ChangeBird", 1f, Random.Range(1f, 1f));
            seq1 = true;
            Invoke("Sequence1", 1f);
            Invoke("Sequence2", 6f);
            Invoke("Sequence3", 12f);
            Invoke("Sequence4", 18f);
            Invoke("Sequence5", 24f);
        }
    }

    void Update(){
        // for testing
        if (Input.GetKeyDown(KeyCode.Space)) { ChangeBird(); }
    }

    public void HideAllBirds(){
        for (int i = 0; i < Birds.Length; i++) {
            Birds[i].SetActive(false);
        }
    }

    // Random Sprite
    public void ChangeBirdSprite(){
        index = Random.Range(0, BirdSprite.Length);
    }

    #region Spawn Birds

    public void SpawnBirds() {
        HideAllBirds();
        Birds[Random.Range(0,7)].SetActive(true);
    }

    public void SpawnBirds2(){
        HideAllBirds();
        Birds[Random.Range(0, 7)].SetActive(true);
        Birds[Random.Range(0, 7)].SetActive(true);
        print("2");
    }

    public void SpawnBirds3()
    {
        HideAllBirds();
        Birds[Random.Range(0, 7)].SetActive(true);
        Birds[Random.Range(1, 7)].SetActive(true);
        Birds[Random.Range(2, 7)].SetActive(true);
        print("3");
    }

    public void SpawnBirds4()
    {
        HideAllBirds();
        Birds[Random.Range(0, 1)].SetActive(true);
        Birds[Random.Range(2, 7)].SetActive(true);
        Birds[Random.Range(3, 7)].SetActive(true);
        Birds[Random.Range(4, 7)].SetActive(true);
        print("4");
    }

    public void SpawnBirds5()
    {
        HideAllBirds();
        Birds[Random.Range(0, 2)].SetActive(true);
        Birds[Random.Range(1, 4)].SetActive(true);
        Birds[Random.Range(3, 7)].SetActive(true);
        Birds[Random.Range(5, 7)].SetActive(true);
        Birds[Random.Range(0, 1)].SetActive(true);
        Birds[Random.Range(2, 4)].SetActive(true);
        Birds[Random.Range(3, 7)].SetActive(true);
        Birds[Random.Range(5, 7)].SetActive(true);
        print("5");
    }

    #endregion

    #region Sequence Functions

    public void Sequence1() {
        if (seq1 == true) {
            InvokeRepeating("SpawnBirds",0f, Random.Range(0.9f, 1f));    
        }
    }

    public void Sequence2()
    {
        HideAllBirds();
        CancelInvoke("SpawnBirds");
        seq1 = false;
        seq2 = true;

        if (seq2 == true)
        {
            InvokeRepeating("SpawnBirds2", 0f, Random.Range(0.9f, 1f));
        }
    }

    public void Sequence3()
    {
        HideAllBirds();
        CancelInvoke("SpawnBirds2");
        seq1 = false;
        seq2 = false;
        seq3 = true;

        if (seq3 == true){
            InvokeRepeating("SpawnBirds3", 0f, Random.Range(0.9f, 1f));
        }
    }

    public void Sequence4()
    {
        HideAllBirds();
        CancelInvoke("SpawnBirds3");
        seq3 = false;
        seq4 = true;

        if (seq4 == true)
        {
            InvokeRepeating("SpawnBirds4", 0f, Random.Range(0.9f, 1f));
        }
    }

    public void Sequence5()
    {
        HideAllBirds();
        CancelInvoke("SpawnBirds4");
        seq4 = false;
        seq5 = true;

        if (seq5 == true)
        {
            InvokeRepeating("SpawnBirds5", 0f, Random.Range(0.9f, 1f));
        }
    }

    #endregion


    public void ChangeBird(){
        ShowRandomBird();
    }

    public void ShowRandomBird(){
        rand = Random.Range(0, 7);
        switch (rand)
        {
            case 0:Birds[0].SetActive(true);

                Birds[1].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 1:
                Birds[1].SetActive(true);

                Birds[0].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 2:
                Birds[2].SetActive(true);

                Birds[1].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 3:
                Birds[3].SetActive(true);

                Birds[1].SetActive(false);
                Birds[2].SetActive(false);
                Birds[0].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 4:
                Birds[4].SetActive(true);

                Birds[0].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[1].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 5:
                Birds[5].SetActive(true);

                Birds[0].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[1].SetActive(false);
                Birds[6].SetActive(false);
                Birds[7].SetActive(false);
                break;


            case 6:
                Birds[6].SetActive(true);

                Birds[0].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[1].SetActive(false);
                Birds[7].SetActive(false);
                break;

            case 7:
                Birds[7].SetActive(true);

                Birds[0].SetActive(false);
                Birds[2].SetActive(false);
                Birds[3].SetActive(false);
                Birds[4].SetActive(false);
                Birds[5].SetActive(false);
                Birds[6].SetActive(false);
                Birds[1].SetActive(false);
                break;

        }
    }

}

