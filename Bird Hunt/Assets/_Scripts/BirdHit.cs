﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BirdHit : MonoBehaviour
{

    [SerializeField] GameController gameController;
   

    public Text ScoreFeedBack;
    public Animator feedbackAnim;

    public int point_score;

    private void OnMouseDown(){
        gameObject.SetActive(false);
        gameController.AddPoint(point_score);
        gameController.PlayGunShot();

        //ScoreFeedBack.text = point_score.ToString();
    }

   
}
