﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{

    public float m_time = 35f;
    public float C_time = 16f;
    public bool GameOver = true;

    public Text timer_Text;
    public Text ButtonText;

    public GameObject GameOverObj,Obj2,Obj3,Obj4,Obj5;
    public SpawnManager1 spawnManager;

   
    private void Update(){
        if (!GameOver) {
            m_time -= Time.deltaTime;
            ButtonText.text = "CONTINUE";
            timer_Text.text = m_time.ToString("F0");
        }
        if (m_time <= 0) {
            spawnManager.HideAllBirds();
            Time.timeScale = 0;
            GameOverObj.SetActive(true);
            Obj2.SetActive(false);
            Obj3.SetActive(false);
            Obj4.SetActive(false);
        }
    }

    public void RollCredits() {
        InvokeRepeating("Countdown", 0f, Time.deltaTime);
    }

    public void Countdown() {
        C_time -= Time.deltaTime;
        if (C_time <= 0) {
            Obj5.SetActive(false);
            CancelInvoke("Countdown");
            C_time = 16f;
        }
    }
}
